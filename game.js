var canvas = document.getElementById('canvas');
var scorebox = document.getElementById('score');
var ctx = canvas.getContext('2d');

var canvas_bg_color = '#000000';
canvas.width = window.outerWidth;
canvas.height = window.outerHeight;

game_speed = 10;

function randomX() {
	return Math.floor(canvas.width * Math.random());
}

function randomY() {
	return Math.floor(canvas.height * Math.random());
}

function randomW() {
	// return Math.floor(canvas.width * Math.random());
	var r = Math.floor(Math.random() * 100);
	if (r > 50) {
		return 10;
	} else {
		return 10;
	}
	// return r;
}

function randomz() {
	// return Math.floor(canvas.width * Math.random());
	var r = Math.floor(Math.random() * 100);
	if (r > 50) {
		return 1;
	} else {
		return 10;
	}
	// return r;
}

function randomH() {
	// return Math.floor(canvas.height * Math.random());
	return 50;
}

// function semiRandomColor() {
// 	Math.floor(Math.random * 100)
// 	return 
// }

function makeSprite() {
	var mySprite = {
		width: randomz(),
		height: randomz(),
		o_width: 30,
		o_height: 30,
		x: randomX(),
		y: randomY(),
		speed: 100,
		color: '#000000',
		dead: false
	};
	return mySprite;
}

var mySprites = [];

mySprites.push(makeSprite());

var multiply_every_sec = 0;
var active_sprite = 0;


function update(mod) {
	if (Object.keys(keysDown).length === 0) {
		if (mySprites[active_sprite].width > mySprites[active_sprite].height) {
			mySprites[active_sprite].x -= mySprites[active_sprite].speed * mod;
		} else {
			mySprites[active_sprite].y += mySprites[active_sprite].speed * mod;
		}
	}
	

	if (37 in keysDown) { // left
		mySprites[active_sprite].x -= mySprites[active_sprite].speed * mod;
	}
	if (38 in keysDown) { // up
		mySprites[active_sprite].y -= mySprites[active_sprite].speed * mod;
	}
	if (39 in keysDown) { // right
		mySprites[active_sprite].x += mySprites[active_sprite].speed * mod;
	}
	if (40 in keysDown) { // down
		mySprites[active_sprite].y += mySprites[active_sprite].speed * mod;
	}
}


function render(bg_color, dude_color) {

	ctx.fillStyle = bg_color;
	ctx.fillRect(0, 0, canvas.width, canvas.height);

	mySprites.forEach(function(sprite, index) {

		if (sprite.dead) {
			ctx.fillStyle = sprite.color;
			if (sprite.width > 2 || sprite.height > 2) {
				sprite.x += 1;
				sprite.y += 1;
				sprite.width -= 2;
				sprite.height -= 2;
			}

		} else {
			sprite.color = dude_color;
		}

		if (sprite.indexOf == active_sprite) {
			ctx.fillStyle = dude_color;
		} else {
			ctx.fillStyle = sprite.color;
		}

		ctx.fillRect(sprite.x, sprite.y, sprite.width, sprite.height);
	})


	multiply_every_sec += 1;

	if (multiply_every_sec == 200) {
		mySprites.push(makeSprite());
		multiply_every_sec = 0;
		mySprites[mySprites.length-2].dead = true;
		active_sprite += 1;
		score.innerHTML = mySprites.length;
	}

}


function run() {
	update((Date.now() - time) / 1000);
	render(
		// '#'+Math.floor(Math.random()*16777215).toString(16),
		canvas_bg_color,
		'#'+Math.floor(Math.random()*16777215).toString(16)
	);

	time = Date.now();
}


var time = Date.now();
setInterval(run, game_speed);


var keysDown = {};

window.addEventListener('keydown', function(e) {
	keysDown[e.keyCode] = true;
});

window.addEventListener('keyup', function(e) {
	delete keysDown[e.keyCode];
});